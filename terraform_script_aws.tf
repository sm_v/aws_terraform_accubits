provider "aws" {
region = "us-west-2"
profile = "default"
}

# creating vpc resouce

resource "aws_vpc" "web-vpc" {
   cidr_block = "192.168.0.0/24"
   instance_tenacy = "default"
   tags = {
      Name = "web"
   }
}

# creating public subnets

resource "aws_subnet" "web-public1" {
   vpc_id  = aws_vpc.web-vpc.id
   cidr_block = "192.168.0.0/26"
   availiability_zone = "us-west-2a"
   map_public_ip_on_launch = true
   tags = {
      Name = "web"
   }
}

resource "aws_subnet" "web-public2" {
   vpc_id = aws_vpc.web-vpc.id
   cidr_block = "192.168.0.64/26"
   availiability_zone = "us-west-2b"
   tags = {
      Name = "web"
   }

}

# creating private subnets

resource "aws_subnet" "web-private1" {
   vpc_id = aws_vpc.web-vpc.id
   cidr_block = "192.168.0.128/26"
   availiability_zone = "us-west-2a"
   tags = {
      Name = "web"
   }

resource "aws_subnet" "web-private2" {
   vpc_id = aws_vpc.web-vpc.id
   cidr_block = "192.168.0.192/26"
   availiability_zone = "us-west-2b"
   tags = {
      Name = "web"
   }

# creating internet gateway

resource "aws_internet_gateway" "web-igw"{
   vpc_id = aws_vpc.web-vpc.id
   tags = {
     Name = "web"
   }
}

# creating elastic ip

resource "aws_eip" "web-eip" {
   vpc      = true
}

# creating NAT gateway

resource "aws_nat_gateway" "web-nat" {
  allocation_id = aws_eip.web-eip.id
  subnet_id     = aws_subnet.web-public1.id
}

# creating public route table

resource "aws_route_table" "web-route-public" {
   vpc_id = aws_vpc.web-vpc.id
   route {
     cidr_block = "0.0.0.0/0
     gateway_id = aws_internet_gateway.web-igw.id
   }
   tags = {
     Name = "web"
   }
}

# attaching route table with subnets


resource "aws_route_table_association" "web-public-route-1" {
   subnet_id = aws_subnet.web-public1.id
   route_table_id = aws_route_table.web-route-public.id
}

resource "aws_route_table_association" "web-public-route-2" {
   subnet_id = aws_subnet.web-public2.id
   route_table_id = aws_route_table.web-route-public.id
}

# creating private route table

resource "aws_route_table" "web-route-private" {
   vpc_id = aws_vpc.web-vpc.id
   route {
     cidr_block = "0.0.0.0/0
     nat_gateway_id = aws_nat_gateway.web-nat.id
   }
   tags = {
     Name = "web"
   }
}

# attaching route table with subnets

resource "aws_route_table_association" "web-private-route-1" {
   subnet_id = aws_subnet.web-private1.id
   route_table_id = aws_route_table.web-route-private.id
}

resource "aws_route_table_association" "web-private-route-2" {
   subnet_id = aws_subnet.web-private2.id
   route_table_id = aws_route_table.web-route-private.id
}

# creating security group for load-balancers


resource "aws_security_group" "web-sg" {
   name = "web-sg"
   description = "Allow traffic for HTTP and HTTPS"
   vpc_id = aws_vpc.web-vpc.id
   ingress {
     description = "HTTP"
     from_port = 80
     to_port = 80
     protocol = "tcp"
     cidr_blocks = ["0.0.0.0/0"]
   }
   ingress {
     description = "HTTPS"
     from_port = 443
     to_port = 443
     protocol = "tcp"
     cidr_blocks = ["0.0.0.0/0"]
   }
   egress {
     from_port = 0
     to_port = 0
     protocol = "-1"
     cidr_blocks = ["0.0.0.0/0"]
   tags = {
     Name = web
   }

}
# creating security group for ec-2

resource "aws_security_group" "web-ec2-sg" {
   name = "web-sg"
   description = "Allow traffic for HTTP and HTTPS"
   vpc_id = aws_vpc.web-vpc.id
   ingress {
     description = "HTTP"
     from_port = 80
     to_port = 80
     protocol = "tcp"
     security_groups = [aws_security_group.web-sg.id]
   }
   ingress {
     description = "HTTPS"
     from_port = 443
     to_port = 443
     protocol = "tcp"
     security_groups = [aws_security_group.web-sg.id]
   }
   egress {
     from_port = 0
     to_port = 0
     protocol = "-1"
     cidr_blocks = [0.0.0.0/0]
   tags = {
     Name = web
   }

}

# creating load balancers

resource "aws_lb" "web-lb" {
   name = "web-lb"
   internal = false
   load_balancer_type = "application"
   security_groups = [aws_security_group.web-sg.id]
   subnets = [aws_subnet.web-public1.id, aws_subnet.web-public2.id]
   tags = {
     Name = "web"
   }
}

# creating ec2 instances

resource "aws_instance" "web-app-1" {
   ami = "ami-0b84c6433cdbe5c3e"
   instance_type = "t2.micro"
   associate_public_ip_address = true
   subnet_id = aws_subnet.web-public.id
   vpc_security_group_ids = [aws_security_group.web-ec2-sg.id]
   key_name = "   "
   tags = {
     Name = web
   }
}

resource "aws_instance" "web-app-2" {
   ami = "ami-0b84c6433cdbe5c3e"
   instance_type = "t2.micro"
   associate_public_ip_address = true
   subnet_id = aws_subnet.web-public.id
   vpc_security_group_ids = [aws_security_group.web-ec2-sg.id]
   key_name = "   "
   tags = {
     Name = web
   }
}

# creating rds db_subnet groups

resource "aws_db_subnet_group" "web-db-subnet" {
   name = "web-db-subnet"
   subnet_ids = [aws_subnet.web-private1.id, aws_subnet.private2.id]
   tags = {
     Name = "web"
   }
}

# creating rds db_instance

resource "aws_db_instance" "web-db" {
   allocted_storage = 10
   engine = "mysql"
   instance_class = db.t3.micro"
   name = "web-db"
   username = "rdsuser"
   password = "sdraW7suser!"
   db_subnet_group_name = aws_db_subnet_group.web-db-subnet.id
   tags = {
     Name = web
   }
}

# creating s3 bucket

resource "aws_s3_bucket" "web-bucket {
   bucket = "webbucket-12200306"
   versioning {
     enabled =true
   }
   cors_rule {
        allowed_headers = ["\*"]
        allowed_methods = ["PUT","POST"]
        allowed_origins = ["\*"]
        expose_headers = ["ETag"]
        max_age_seconds = 3000
    }
   tags = {
     Name = web
   }

resource “aws_s3_bucket_object” “web-bucketobject” {
bucket = “${aws_s3_bucket.web-bucket.bucket}”
}
locals{
  s3_origin_id = aws_s3_bucket.web-bucket.id
}

# creating cloudfront with s3

resource "aws_cloudfront_distribution" "web-cloudfront" {
   depends_on = [ aws_s3_bucket_object.web-bucketobject ]
    origin {
        domain_name = "${aws_s3_bucket.web-bucket.website_endpoint}"
        origin_id = "S3-${aws_s3_bucket.web-bucket}"
 
        custom_origin_config {
            http_port = 80
            https_port = 443
            origin_protocol_policy = "match-viewer"
            origin_ssl_protocols = ["TLSv1", "TLSv1.1", "TLSv1.2"]
        }
    }
    default_root_object = "index.html"
    enabled = true
    custom_error_response {
        error_caching_min_ttl = 3000
        error_code = 404
        response_code = 200
        response_page_path = "/index.html"
    }
    default_cache_behavior {
        allowed_methods = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
        cached_methods = ["GET", "HEAD"]
        target_origin_id = "S3-${aws_s3_bucket.web-bucket}"
        forwarded_values {
            query_string = true
        }
        viewer_protocol_policy = "allow-all"
        min_ttl = 0
        default_ttl = 3600
        max_ttl = 86400
    }
    price_class = "PriceClass_100"
    restrictions {
        geo_restriction {
            restriction_type = "none"
        }
    }
    viewer_certificate {
        cloudfront_default_certificate = true
    }
}